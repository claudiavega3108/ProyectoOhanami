import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/vista_ronda1.dart';
import 'package:principal/vistas/componentes/vista_ronda2.dart';
import 'package:principal/vistas/componentes/vista_ronda3.dart';

class BlocVista extends StatelessWidget {
  const BlocVista({ Key? key, required this.partida }) : super(key: key);
  final Partida partida;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BlocPrincipal(partida),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(colorPrincipal),
            title: Text('Rondas'),
          ),
          body: AdmonVistas(partida: partida,),
        ),
      ),
    );
  }
}

class AdmonVistas extends StatelessWidget {
  const AdmonVistas({ Key? key, required this.partida }) : super(key: key);
  final Partida partida;


  @override
  Widget build(BuildContext context) {
    final estados = context.watch<BlocPrincipal>().state;
    return estados.map(
      Ronda1: (ronda1) => VistaRonda1(partida: ronda1.partida,), 
      Ronda2: (ronda2) => VistaRonda2(partida: ronda2.partida,), 
      Ronda3: (ronda3) => VistaRonda3(partida: ronda3.partida,),
      );
  }
}