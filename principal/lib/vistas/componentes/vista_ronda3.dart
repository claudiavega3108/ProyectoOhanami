import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/bloc_principal/eventos.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/local/local.dart';
import 'package:principal/vistas/componentes/componentes.dart';
import 'package:principal/vistas/componentes/estilos.dart';
import 'package:provider/src/provider.dart';

class VistaRonda3 extends StatefulWidget {
  const VistaRonda3({ Key? key, required this.partida}) : super(key: key);
  final Partida partida; 

  @override
  State<VistaRonda3> createState() => _VistaRonda3State(partida);
}

class _VistaRonda3State extends State<VistaRonda3> {

 Partida partida;
  _VistaRonda3State(this.partida);

  List<TextEditingController> _cartasAzules = [];
  List<TextEditingController> _cartasVerdes = [];
  List<TextEditingController> _cartasRosas = [];
  List<TextEditingController> _cartasNegras = [];
  Local local = Local();

  Future<void> validarCartas(index) async {
    try {
      List<pRonda3> lista = [];
      for (var i = 0; i < index; i++) {
        print("jugador " + (i + 1).toString());
        int azules =int.parse(_cartasAzules[i].text);
        int verdes =int.parse(_cartasVerdes[i].text);
        int rosas =int.parse(_cartasRosas[i].text);
        int negras =int.parse(_cartasNegras[i].text);
        pRonda3 ronda3 = pRonda3(
          jugador: partida.jugadores.elementAt(i), 
          cuantasAzules: azules,
          cuantasVerdes: verdes,
          cuantasRosas: rosas,
          cuantasNegras: negras, 
          );
        lista.add(ronda3);
      }
    List<PuntuacionJugador> j;
      partida.cartasRonda3(lista);
      bool bandera = await actualizar(partida: partida);
      if (bandera == true) {
      Navigator.push(context, MaterialPageRoute( builder: (context) => VistaDetallePartida(partida: partida,)));
      }
    } on Exception catch (e){
      _mensaje(e.toString());
    }
  }

  Future<bool> actualizar({required Partida partida})async{
    bool bandera = false;
    Local local = Local();
    bandera = await local.registrarPartida(partida: partida);
    return bandera;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Center(
            child: Text('Ronda 3',
            style: estiloTituloRondas,
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: 80),
            itemCount: partida.jugadores.length + 1,
            itemBuilder: (BuildContext context, int index) {
            return index == partida.jugadores.length
            ? Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () async {
                    validarCartas(index);
                  },
                    child: Text("Aceptar"),
                    style: estiloElevatedButtonAceptar,
                  ),
              ),
              )
              : Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(partida.jugadores.elementAt(index).nombre.toString(),
                                style: estiloNombreRondas,
                            ),
                          ],
                        ),
                      ),
                      _cartas(index),
                    ],
                  ),
                );
              }
            ),
          ),
        ],
      ),
    );
  }

  _cartas(index){
    _cartasAzules.add(TextEditingController());
    _cartasVerdes.add(TextEditingController());
    _cartasRosas.add(TextEditingController());
    _cartasNegras.add(TextEditingController());
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasAzules[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                focusColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Azules',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                ),
              ),
            ),
          ),
        ), 
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasVerdes[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                focusColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Verdes',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasRosas[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                focusColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Rosas',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
               controller: _cartasNegras[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                focusColor: Color(colorPrincipal),
                hintStyle: TextStyle(color: Color(colorPrincipal)),
                counterText: "",
                labelText: 'Negras',
                border: borderInputsAnadir,
                 enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _mensaje(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}