import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/bloc_principal/eventos.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/estilos.dart';
import 'package:provider/src/provider.dart';

class VistaRonda2 extends StatefulWidget {
  const VistaRonda2({ Key? key, required this.partida}) : super(key: key);
  final Partida partida; 

  @override
  State<VistaRonda2> createState() => _VistaRonda2State(partida);
}

class _VistaRonda2State extends State<VistaRonda2> {
  Partida partida;
  _VistaRonda2State(this.partida);

  List<TextEditingController> _cartasAzules = [];
  List<TextEditingController> _cartasVerdes = [];
  var cAzules = TextEditingController();
  var cVerdes = TextEditingController();

  void validarCartas(index){
      try {
        List<pRonda2> lista = [];
        for (var i = 0; i < index; i++) {
          print("jugador " + (i + 1).toString());
          int azules =int.parse(_cartasAzules[i].text);
          int verdes =int.parse(_cartasVerdes[i].text);
          pRonda2 ronda2 = pRonda2(
            jugador: partida.jugadores.elementAt(i), 
            cuantasAzules: azules,
            cuantasVerdes: verdes, 
          );
          lista.add(ronda2);
        }
          partida.cartasRonda2(lista);
          context.read<BlocPrincipal>().add(DespuesRonda3(partida: partida));
      } on Exception catch (e){
        _mensaje(e.toString());
      }
    }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Center(
            child: Text('Ronda 2',
            style: estiloTituloRondas,
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: 80),
            itemCount: partida.jugadores.length + 1,
            itemBuilder: (BuildContext context, int index) {
            return index == partida.jugadores.length
            ? Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () {
                          validarCartas(index);
                      },
                      child: Text("Siguiente ronda"),
                      style: estiloElevatedButtonAceptar,),
                ),
              )
              : Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(partida.jugadores.elementAt(index).nombre.toString(),
                                style: estiloNombreRondas,
                            ),
                          ],
                        ),
                      ),
                      _cartas(index),
                    ],
                  ),
                );
              }
            ),
          ),
        ],
      ),
    );
  }


  _cartas(index){
    _cartasAzules.add(TextEditingController());
    _cartasVerdes.add(TextEditingController());
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasAzules[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                hoverColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Azules',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ), 
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasVerdes[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                focusColor: Color(colorPrincipal),
                hoverColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Verdes',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ) 
      ],
    );
  }

  void _mensaje(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}