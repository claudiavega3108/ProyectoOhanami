import 'package:db_paquete/db_paquete.dart';
import 'package:flutter/material.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/bloc_principal/estados.dart';
import 'package:principal/bloc_principal/eventos.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/componentes.dart';
import 'package:principal/vistas/componentes/estilos.dart';
import 'package:provider/src/provider.dart';

class VistaIniciandome extends StatefulWidget {
  const VistaIniciandome({Key? key}) : super(key: key);

  @override
  State<VistaIniciandome> createState() => _VistaIniciandomeState();
}

class _VistaIniciandomeState extends State<VistaIniciandome> {
    void entrarSinConexion() async {
    Jugadores jugador = Jugadores(
        nombre: '',
        correo: '',
        contrasena: '',
        partida: []);
    //local.registrarJugadores(j: jugador);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => VistaPartidas()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Ohanami'),
        backgroundColor: Color(colorPrincipal),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: Image.asset('assets/images/ohanami.png'),
          ),
          const Padding(
            padding: EdgeInsets.all(25.0),
            child: Center(
              child: Text("Bienvenido", 
              style: TextStyle(
                fontSize: 35.0,
                fontWeight: FontWeight.w900,
                color: Color(bienvenidaMensaje)
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: estiloElevatedButton,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => VistaRegistrar()),
                );
            },
              child: Text('Registrate'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
            child: ElevatedButton(
              style: estiloElevatedButton,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => VistaInicioSesion()),
                  );
              },
              child: Text('Inicia Sesi\u00F3n'),
            ),
          ),
          ElevatedButton(
            style: estiloElevatedButton,
            onPressed: () {
              entrarSinConexion();
            },
            child: Text('Sin conexi\u00F3n'),
          ),
        ],
      ),
    );
  }
}