import 'dart:async';

import 'package:db_paquete/db_paquete.dart';
import 'package:flutter/material.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/local/local.dart';
import 'package:principal/vistas/componentes/componentes.dart';

class Splash extends StatefulWidget {
  const Splash({ Key? key,}) : super(key: key);


  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  bool badera = false;
  TextEditingController nombre = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController clave = TextEditingController();
  Mongo mongo = Mongo();
  Local local = Local();
  @override
  void initState() {
    validacion().whenComplete(() async {
      Timer(Duration(),
      (){
        badera == false ? 
      Navigator.push( context, MaterialPageRoute( builder: (context) => VistaInicioSesion())) :
      Navigator.push( context, MaterialPageRoute(builder: (context) => VistaPartidas()));
      });
    });
    
    super.initState();
  }

  Future validacion() async{
    //local.eliminarUsuario();
    bool banderaMogo = await mongo.iniciarMongo();
    bool banderaL = await local.valExisteJugador();

    if (banderaMogo == true && banderaL == true) {
      Jugadores usuario = await local.recuperarJugadores();
      /*
      //checkv = await mongo.registradoUsuario(usuario: usuario);

      if (checkv == true) {
        print("Sos");
        /*
        //Usuario u = await mongo.recuperarUsuario(usuario: usuario);
        checkv = await local.registrarUsuario(usuario: u);
        setState(() {
          check = checkv;
        });*/
    }
      print("No se ");*/
      
      setState(() {
        badera = true;
      });
    }
    if (banderaMogo == false && banderaL == true) {
      setState(() {
        badera = banderaL;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(imagenLogo),
            const Padding(
              padding: EdgeInsets.all(30.0),
              child: CircularProgressIndicator(
                color: Color(colorPrincipal),
              ),
            )
          ],
        ),
      ),
    );
  }
}
