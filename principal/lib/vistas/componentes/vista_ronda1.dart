import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/bloc_principal/eventos.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/estilos.dart';
import 'package:provider/src/provider.dart';

class VistaRonda1 extends StatefulWidget {
  const VistaRonda1({ Key? key, required this.partida}) : super(key: key);
  final Partida partida; 

  @override
  State<VistaRonda1> createState() => _VistaRonda1State(partida);
}

class _VistaRonda1State extends State<VistaRonda1> {
  Partida partida;
  _VistaRonda1State(this.partida);

  List<TextEditingController> _cartasAzules = [];

  void validarCartas(index){
    try {
      List<pRonda1> lista = [];
      for (var i = 0; i < index; i++) {
        print("jugador " + (i + 1).toString());
        int azules =int.parse(_cartasAzules[i].text);
        print(_cartasAzules[i].text);
        pRonda1 ronda1 = pRonda1(
          jugador: partida.jugadores.elementAt(i), 
          cuantasAzules: azules, 
        );
        lista.add(ronda1);
      }
      partida.cartasRonda1(lista);
      //print(partida.puntuacionesRonda1[0].cuantasAzules.toString()); 
      context.read<BlocPrincipal>().add(DespuesRonda2(partida: partida));
  } on Exception catch (e) {
    if (e.runtimeType == FormatException) {
    return _mensae("Llene los campos correctamente.");
    }
      _mensae(e.toString());
    }
  }

  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Center(
            child: Text('Ronda 1',
            style: estiloTituloRondas,
            ),
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: 80),
            itemCount: partida.jugadores.length + 1,
            itemBuilder: (BuildContext context, int index) {
            return index == partida.jugadores.length
            ? Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () {
                          validarCartas(index);
                      },
                      child: Text("Siguiente ronda"),
                      style: estiloElevatedButtonAceptar,),
                ),
              )
              : Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text(partida.jugadores.elementAt(index).nombre.toString(),
                                  style: estiloNombreRondas,
                              ),
                            ),
                          ],
                        ),
                      ),
                      _cartas(index),
                    ],
                  ),
                );
              }
            ),
          ),
        ],
      ),
    );
  }


  _cartas(index){
    _cartasAzules.add(TextEditingController());
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              validator: (value) => value == null || value.isEmpty ? 'Llena los campos': null,
              controller: _cartasAzules[index],
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              maxLength: 2,
              decoration: InputDecoration(
                hintStyle:TextStyle(color: Color(colorPrincipal)),
                focusColor: Color(colorPrincipal),
                hoverColor: Color(colorPrincipal),
                counterText: "",
                labelText: 'Azules',
                border: borderInputsAnadir,
                enabledBorder:  OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(colorPrincipal).withOpacity(0.5), 
                  )
                )
              ),
            ),
          ),
        ), 
      ],
    );
  }
  void _mensae(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}
