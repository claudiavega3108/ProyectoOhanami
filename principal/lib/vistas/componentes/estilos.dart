import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:principal/constantes/colores.dart';

var estiloElevatedButton =  ElevatedButton.styleFrom(
  primary: Color(colorBotones),
  onPrimary: Colors.white,
  fixedSize: Size(150, 40),
  textStyle: TextStyle(
    color: Colors.white,
    fontSize: 20,
  ),
);

var estiloThemeData = ThemeData(
  primaryColor: Color(colorPrincipal),
  inputDecorationTheme: InputDecorationTheme(
    //focusColor: Color(rosaInputs),
    labelStyle: TextStyle(color: Color(colorInputs)),
    hintStyle: TextStyle(color: Color(colorInputs)),
  )
);

var estiloEnableBorder = OutlineInputBorder(
  //borderRadius: BorderRadius.circular(.0),
  borderSide: BorderSide(
    //color: Color(colorInputs).withOpacity(.7),
    width: 2.0,
  ),
);

var estiloOutlineBorder = OutlineInputBorder(borderSide: BorderSide(color: Color(colorInputs)));

var estidoloEBAnadir = ElevatedButton.styleFrom(
  primary: Color(colorBotones),
  onPrimary: Colors.white,
  fixedSize: Size(70, 20),
  textStyle: TextStyle(
    color: Colors.white,
    fontSize: 20,
  ),
);

var estiloElevatedButtonAceptar = ElevatedButton.styleFrom(
  primary: Color(colorBotones),
  onPrimary: Colors.white,
  fixedSize: Size(120, 20),
  textStyle: TextStyle(
    color: Colors.white,
    fontSize: 20,
  ),
);

var estiloTituloRondas = TextStyle(
  color: Color(colorInputs),
  fontSize: 20,
  fontWeight: FontWeight.bold
);


 var estiloTitulos = TextStyle(
  color: Color(colorInputs),
  fontSize: 30,
  fontWeight: FontWeight.bold
);
var estiloTexto = TextStyle(
  color: Color(colorInputs),
);

var borderInputsAnadir = OutlineInputBorder(
  //borderRadius: BorderRadius.circular(.0),
  borderSide: BorderSide(
    color: Color(colorInputs).withOpacity(.7),
    width: 2.0,
  ),
);

var estiloNombresDetalle = TextStyle(
  color: Color(colorInputs),
  fontWeight: FontWeight.w700,
  fontSize: 18.0,
);

var estiloNombreRondas = TextStyle(
  color: Color(colorInputs),
  fontWeight: FontWeight.w600,
  fontSize: 20.0
);

var estiloPuntuacionesAzules = TextStyle(
  color: Color(colorAzulR2),
  fontWeight: FontWeight.w600,
  fontSize: 15.0
);

var estiloPuntuacionesVerdes = TextStyle(
  color: Color(colorVerdeR3),
  fontWeight: FontWeight.w600,
  fontSize: 15.0
);
var estiloPuntuacionesRosas = TextStyle(
  color: Color(colorRosaR3),
  fontWeight: FontWeight.w600,
  fontSize: 15.0
);
var estiloPuntuacionesNegras = TextStyle(
  color: Color(colorNegroR3),
  fontWeight: FontWeight.w600,
  fontSize: 15.0
);

      

