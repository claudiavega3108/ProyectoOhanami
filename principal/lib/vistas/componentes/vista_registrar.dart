import 'dart:io';
import 'dart:ui';

import 'package:db_paquete/db_paquete.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/local/local.dart';
import 'package:provider/src/provider.dart';
import 'package:principal/vistas/componentes/componentes.dart';

class VistaRegistrar extends StatefulWidget {
  const VistaRegistrar({Key? key}) : super(key: key);

  @override
  State<VistaRegistrar> createState() => _VistaRegistrarState();
}

class _VistaRegistrarState extends State<VistaRegistrar> {
  bool bandera = false;
  bool _mostrarContra = false;
  bool _mostrarConfContra = false;
  bool registrar = false;
  bool existe = false;
  final tfNombre = TextEditingController();
  final tfCorreo = TextEditingController();
  final tfContrasena = TextEditingController();
  final tfConfContra = TextEditingController();
  Local local = Local();
  Mongo mongo = Mongo();
  final _formKey = GlobalKey<FormState>();
 @override
  void initState() {
    super.initState();
  }

  Future validar() async {
    //local.eliminarJugadores();
    bool mongocheck = await mongo.iniciarMongo();
    bool checkv = await local.valExisteJugador();

    if (mongocheck == true && checkv == true) {
      Jugadores jugador = await local.recuperarJugadores();
      checkv = await mongo.valExisteJugador(jugador: jugador);
      if (checkv == true) {
        Jugadores j = await mongo.recuperarJugador(j: jugador);
        checkv = await local.registrarJugadores(j: j);
        setState(() {
          bandera = checkv;
        });
      }
      setState(() {
        bandera = true;
      });
    }
    if (mongocheck == false && checkv == true) {
      setState(() {
        bandera = checkv;
      });
    }
  }

  void registrarJugadores() async {
    print('Entro validarRegistro');
    Jugadores j = Jugadores(
        nombre: tfNombre.text,
        correo: tfCorreo.text,
        contrasena: tfContrasena.text,
        partida: []);
    bool nombreUsuarioEnUso = await mongo.valExisteJugador(jugador: j);
    if (nombreUsuarioEnUso == true) {
      print("Este nombre de usuario ya esta registrado");
    } else {
      Local local = Local();
      var existeJugador = await local.valExisteJugador();
      if(existeJugador){
        bool usuarioLocalActualizado =
          await local.cambiarJugador(usuarioNuevo: j);
      if (usuarioLocalActualizado == true) {
        Jugadores jugadorLocal = await local.recuperarJugadores();
        print("Usuario actualizado en local");

        bool usuarioRegistradoConExito =
            await mongo.registrarJugadores(jugador: jugadorLocal);
        usuarioRegistradoConExito == true
            ? print("usuario registrado en mongo")
            : print("hubo un error al registrar");
      }
      }
      else{
        await mongo.registrarJugadores(jugador: j);
        await local.registrarJugadores(j: j);
        Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const VistaPartidas()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: estiloThemeData,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => VistaIniciandome()));
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text("Registrar"),
        backgroundColor: Color(colorPrincipal),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0, top: 25.0),
                  child: Image.asset(
                    imagenLogo,
                    width: 120,
                    height: 90,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Reg\u00EDstrate',
                      style: estiloTitulos,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: TextFormField(
                                controller: tfNombre,
                                validator: (value) => value == null || value.isEmpty ? 'Ingrese un nombre' : null,
                                decoration: InputDecoration(
                                  //icon: Icon(Icons.account_circle),
                                  hintText: 'maria',
                                  labelText: 'Usuario',
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(colorInputs)
                                    )
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(colorInputs)
                                    ),
                                  ),
                                  //enabledBorder: estiloEnableBorder,
                                  focusColor: Color(colorPrincipal),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color(colorPrincipal))),
                                  hintStyle:
                                      TextStyle(color: Color(colorInputs)),
                                ),
                                onChanged: (value) {},
                              ),
                            ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                              controller: tfCorreo,
                              validator: (value) => value == null || value.isEmpty || !value.contains("@") ? 'Correo Invalido' : null,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                //icon: Icon(Icons.email),
                                hintText: 'maria@gmail.com',
                                labelText: 'Correo',
                                border: estiloOutlineBorder,
                                focusColor: Color(colorInputs),
                                //enabledBorder: estiloEnableBorder,
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(colorInputs)
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(colorInputs)
                                  ),
                                ),
                                hintStyle:
                                    TextStyle(color: Color(colorInputs)),
                              ),
                              onChanged: (value) {},
                            ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                                child: TextFormField(
                                    obscureText: _mostrarContra ? false : true,
                                    controller: tfContrasena,
                                    validator: (value) => value == null || value.isEmpty ? 'Ingrese una contra\u00F1a' : null,
                                    keyboardType: TextInputType.emailAddress,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.deny(
                                          RegExp(r"\s\b|\b\s"))
                                    ],
                                    decoration: InputDecoration(
                                      //icon: Icon(Icons.password),
                                      hintText: 'Contrase\u00F1a',
                                      labelText: 'Contrase\u00F1a',
                                      //prefix: Icon(Icons.password_rounded),
                                      focusColor: Color(colorInputs),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(colorInputs))),
                                      focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(colorPrincipal)
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(colorInputs)
                                    ),
                                  ),
                                      hintStyle:
                                          TextStyle(color: Color(colorInputs)),
                                      //enabledBorder: estiloEnableBorder,
                                      suffixIcon: IconButton(
                                        onPressed: () => _verContrasena(),
                                        icon: Icon(_mostrarContra
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                      ),
                                    ))),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              obscureText: _mostrarConfContra ? false : true,
                              controller: tfConfContra,
                              validator: (value) => value == null || value.isEmpty ? 'Vuelva a ingresar la contrase\u00F1a' : null,
                              keyboardType: TextInputType.emailAddress,
                              inputFormatters: [
                                FilteringTextInputFormatter.deny(
                                    RegExp(r"\s\b|\b\s"))
                              ],
                              decoration: InputDecoration(
                                //icon: Icon(Icons.password),
                                hintText: 'Confirmar contrase\u00F1a',
                                labelText: 'Confirmar contrase\u00F1a',
                                //prefix: Icon(Icons.password_rounded),
                                focusColor: Color(colorInputs),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(colorInputs))),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(colorPrincipal)
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(colorInputs)
                                  ),
                                ),
                                hintStyle: TextStyle(color: Color(colorInputs)),
                                suffixIcon: IconButton(
                                  onPressed: () => _verConfContra(),
                                  icon: Icon(_mostrarContra
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                ),
                              )
                            )
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: double.infinity,
                            height: 40,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color(colorBotones),
                                onPrimary: Colors.white,
                                //fixedSize: Size(, 40),
                                textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                              onPressed: () async {
                              if(_formKey.currentState!.validate()){
                                registrarJugadores();
                              }
                              },
                              child: Text('Crear cuenta'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }

  bool confirmacionContra(){
  if(tfConfContra.text.toString() == tfContrasena.text.toString()){
    return true;
  }
  return false;
}

  void _verContrasena() {
    setState(() {
      _mostrarContra = !_mostrarContra;
    });
  }

  void _verConfContra() {
    setState(() {
      _mostrarConfContra = !_mostrarConfContra;
    });
  }

  void _mensae(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}
