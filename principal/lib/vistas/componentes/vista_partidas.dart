import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/constantes/colores.dart';
import 'dart:async';
import 'package:principal/local/local.dart';
import 'package:principal/vistas/componentes/componentes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:db_paquete/db_paquete.dart';

class VistaPartidas extends StatefulWidget {
  const VistaPartidas({Key? key}) : super(key: key);

  @override
  VistaPartidasState createState() => VistaPartidasState();
}

class VistaPartidasState extends State<VistaPartidas> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<int> _counter;
  late Future<Jugadores> _jugadores;
  Local local = Local();
  Mongo mongo = Mongo();
  final ScrollController _firstController = ScrollController();

  late Future<Jugadores> jugador;
  @override
  void initState() {
    jugador = local.recuperarJugadores();
    //print(jugador.toString());
    super.initState();
  }

  void subirBaseDeDatos() async {
    Jugadores jugadorL = await local.recuperarJugadores();
    bool bandera = await mongo.iniciarMongo();
    if (bandera == true) {
      bandera = await mongo.valExisteJugador(jugador: jugadorL);
      if (bandera == true) {
        bool checo = await mongo.actualizarPartida(j: jugadorL);
        print("Si se pudo");
        //_mensae('La sincronizacion a la base de datos ha sido exitosa');
      } else {
        bool check2 = await mongo.registrarJugadores(jugador: jugadorL);
        print("Se pudo mejor");
        _mensae('La sincronizacion a la base de datos ha sido exitosa');
      }
    } else {
      print("No hay conexion");
      _mensae('No hay conexion a internet');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(colorPrincipal),
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const VistaAnadir()));
          }
        ),
        drawer: FutureBuilder(
          future: jugador,
          builder: (BuildContext context, AsyncSnapshot<Jugadores> snapshot) {
            return snapshot.hasData
                ? snapshot.hasError
                    ? Text("Error", style:TextStyle(color: Color(colorInputs), fontSize: 25.0, fontWeight: FontWeight.w900))
                    // vista error
                    : Drawer(
                        child: ListView(
                          padding: EdgeInsets.zero,
                          children: [
                            DrawerHeader(
                              decoration: BoxDecoration(
                                color: Color(colorPrincipal),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(snapshot.data!.nombre.toString(),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w500,
                                      ),),
                                    Text(snapshot.data!.correo.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500,
                                      ),),
                                  ],
                                ),
                              ),
                            ),
                            snapshot.data!.nombre.isNotEmpty
                              ? Column(
                                  children: [
                                    ListTile(
                                      leading:Icon(Icons.upload),
                                      title: Text('Subir a base de datos', style:TextStyle(color: Color(colorInputs), fontSize: 20.0, fontWeight: FontWeight.w600)),
                                      onTap: () async {
                                        subirBaseDeDatos();
                                      },
                                    ),
                                    ListTile(
                                      leading: Icon(Icons.logout),
                                      title: Text("Cerrar Sesion", style:TextStyle(color: Color(colorInputs), fontSize: 20.0, fontWeight: FontWeight.w600)),
                                      onTap: () async {
                                        print(
                                            "Estas seguro de eliminar el usuario");
                                        print(
                                            "Se eliminara todo si no sincronizaste al db");
                                        bool check =
                                            await local.eliminarJugadores();
                                        if (check == true) {
                                          print("Se elimino el usuario");
                                        }
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                              const VistaIniciandome()));
                                      },
                                    ),
                                  ],
                                )
                                : ListTile(
                                    title: Text("Registrarse", style:TextStyle(color: Color(colorInputs), fontSize: 25.0, fontWeight: FontWeight.w900)),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => const VistaIniciandome()));
                                  },
                              ),
                          ],
                        ),
                      )
                : CircularProgressIndicator();
            // cargando
          },
        ),
        appBar: AppBar(
          title: Text('Partidas'),
          backgroundColor: Color(colorPrincipal),
        ),
        body: FutureBuilder<Jugadores>(
            future: local.recuperarJugadores(),
            builder:
                (BuildContext context, AsyncSnapshot<Jugadores> snapshot) {
              return snapshot.hasData
                  ?
                  snapshot.hasError
                      ?
                      Center(child: Text("Error al obtener datos", style: TextStyle(color: Color(colorInputs), fontSize: 25.0, fontWeight: FontWeight.w900),))
                      :
                      snapshot.data!.partida.isEmpty
                          ?
                          Center(child: Text("No hay partidas",
                          style: TextStyle(color: Color(colorInputs), fontSize: 25.0, fontWeight: FontWeight.w900),))
                          :
                          _campoDatos(snapshot)
                  :Center(child: const CircularProgressIndicator(color: Color(colorPrincipal),));
          }
        ),
      ),
    );
  }

  _campoDatos(snapshot) {
    return ListView.builder(
      itemCount: snapshot.data!.partida.length,
      itemBuilder: (BuildContext context, int index) {
        int reverseIndex = snapshot.data!.partida.length - 1 - index;
        print(reverseIndex); // verificador
        FasePuntuacion.ronda1;
        FasePuntuacion.ronda2;
        FasePuntuacion.ronda3;
        snapshot.data!.partida[0].puntuaciones(rondas: FasePuntuacion.ronda1);
        List<String> nombres = [];
        for (var i = 0;
            i < snapshot.data!.partida[reverseIndex].jugadores.length;
            i++) {
          nombres.add(snapshot.data!.partida[reverseIndex].jugadores
              .elementAt(i)
              .nombre
              .toString());
        }
        do {
          nombres.add("");
        } while (nombres.length < 4);
        return Container(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(15),
                  margin: const EdgeInsets.fromLTRB(7,10,12,0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(colorPrincipal).withOpacity(.2),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                          text: TextSpan(
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Color(colorInputs),
                            ),
                            children: <TextSpan>[
                              /*TextSpan(text: 'Partida: ', style:  TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                              TextSpan(text: 'id_partida \n', style:  TextStyle( fontSize: 15)),*/
                              /*TextSpan(text: 'Numero de jugadores: ',style:  TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                              TextSpan(text:  '\n  '+ (nombres.length).toString() + '\n'),*/
                              TextSpan(text: 'Jugadores: ',style:  TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                              TextSpan(text: '\n  ' + nombres[0], style:  TextStyle( fontSize: 15)),
                              TextSpan(text: '\n  ' + nombres[1], style:  TextStyle( fontSize: 15)),
                              if(nombres[2].isNotEmpty) TextSpan(text: '\n  ' + nombres[2],style:  TextStyle( fontSize: 15)),
                              if(nombres[3].isNotEmpty) TextSpan(text: '\n  ' + nombres[3], style:  TextStyle( fontSize: 17)),
                          ],
                        ),
                      ) ,
                      Column(
                      children: [
                        ElevatedButton(
                          onPressed: () async{
                            Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VistaDetallePartida(
                                    partida: snapshot
                                        .data!.partida[reverseIndex])));
                          }, 
                          child: Text('Ver Partida'),
                          style: ElevatedButton.styleFrom(
                            primary: Color(colorBotones),
                            onPrimary: Colors.white,
                            fixedSize: Size(150, 20),
                            textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: ()async {
                          await local
                                .eliminarPartida(index: reverseIndex)
                                .whenComplete(() {
                              setState(() {
                                snapshot.data!.partida.removeAt(reverseIndex);
                              });
                            });
                            _mensae('Se elimino la parida');
                          }, 
                          child: Text('Eliminar Partida'),
                              style: ElevatedButton.styleFrom(
                              primary: Color(colorBotones),
                              onPrimary: Colors.white,
                              fixedSize: Size(150, 20),
                              textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                              ),
                            ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }
    );
  }

  void _mensae(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}
