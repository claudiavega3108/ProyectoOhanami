import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/bloc_principal.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/bloc_vista_principal.dart';
import 'package:principal/vistas/componentes/estilos.dart';

class VistaAnadir extends StatefulWidget {
  const VistaAnadir({Key? key}) : super(key: key);

  @override
  _VistaAnadirState createState() => _VistaAnadirState();
}

class _VistaAnadirState extends State<VistaAnadir> {
late int _count;
late String _result;
late bool agregar, agregar2, eliminar, comenzar;

late final List<TextEditingController> _controllers =
    List.generate(4, (i) => TextEditingController());

@override
void initState() {
  super.initState();
  _count = 2;
  _result = '';
  agregar = false;
  eliminar = false;
  comenzar = false;
}

void checar(){
  bool bandera = true;  
  for (var i = 0; i < _count; i++) {
    print(_controllers[i].text.isEmpty);
    if (_controllers[i].text.isEmpty){
      bandera = false;
    } 
  }
  setState(() {
    agregar = bandera;
  });
}    


@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: const Text("Agregar partida"),
      backgroundColor: Color(colorPrincipal),
    ),
    body: Container(
        //alignment: Alignment.topCenter,
        padding: const EdgeInsets.only(top: 20.0),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text('Agrega los nombres de los jugadores',
                style: TextStyle(
                  fontSize: 25,
                  color: Color(colorInputs),
                  fontWeight: FontWeight.w800
                ),
              ),
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: _count,
                itemBuilder: (context, index) {
                  return _row(index);
                }),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _count == 4
                    ? ElevatedButton(
                      style: estidoloEBAnadir,
                        onPressed: null,
                        child: Icon(Icons.add),
                      )
                    : ElevatedButton(
                      style: estidoloEBAnadir,
                        onPressed: () => {
                          if (_count < 4)
                            {
                              setState(() {
                                _count++;
                                agregar = false;
                              }),
                            }
                        },
                        child: Icon(Icons.add),
                      ),
                const SizedBox(width: 5),
                _count == 2 ? ElevatedButton(
                        style: estidoloEBAnadir,
                        onPressed: null,
                        child: Icon(Icons.remove),
                      )
                    : ElevatedButton(
                      style: estidoloEBAnadir,
                        onPressed: () => {
                          if (_count > 2)
                            {
                              if (_count >= 3)
                                {
                                  _controllers[_count - 1].clear(),
                                },
                              setState(() {
                                _count--;
                              }),
                              checar()
                            }
                        },
                        child: Icon(Icons.remove),
                      ),
                      const SizedBox(width: 5),
                      (agregar == false)
                    ? ElevatedButton(
                        style: estiloElevatedButtonAceptar,
                        onPressed: null,
                        child: Text('Aceptar'),
                      )
                    : ElevatedButton(
                      style: estiloElevatedButtonAceptar,
                        onPressed: () {
                          Set<Jugador> jugadores = {};
                          for (var i = 0; i < _count; i++) {
                            jugadores.add(Jugador(nombre: _controllers[i].text));
                          }
                          Partida partida = Partida(jugadores: jugadores);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => BlocVista(partida: partida)));
                        },
                        child: Text('Aceptar'),
                      ),
              ],
            ), 
            Text(_result),
          ],  
        )
      ),
  );
}

  _row(int key) {
    return Row(
      children: [ 
        const SizedBox(),
        Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, bottom: 5.0),
              child: TextFormField(
                  controller: _controllers[key],
                  onChanged: (_) {
                    checar();
                  },
                  maxLength: 15,
                  decoration: InputDecoration(
                    fillColor: Color(colorPrincipal),
                    hoverColor: Color(colorPrincipal),
                    //hintText: 'Jugador ' + (key + 1).toString() + ':',
                    labelText: 'Jugador ' + (key + 1).toString(),
                    counterText: "",
                    border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(colorInputs)
                      )
                    ),
                    //enabledBorder: estiloEnableBorder,
                     focusColor: Color(colorPrincipal),
                     focusedBorder: OutlineInputBorder(
                     borderSide: BorderSide(
                      color: Color(colorPrincipal)
                      )
                     ),
                     hintStyle: TextStyle(color: Color(colorPrincipal),
                     ),
                  ),
              ),
            )
        ),
      ],
    );
  }
}