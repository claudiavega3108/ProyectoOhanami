import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:paquete/paquete.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/componentes.dart';

class VistaDetallePartida extends StatefulWidget {
  const VistaDetallePartida({ Key? key, required this.partida }) : super(key: key);
  final Partida partida;

  @override
  _VistaDetallePartidaState createState() => _VistaDetallePartidaState(partida);
}

class _VistaDetallePartidaState extends State<VistaDetallePartida> {
  Partida partida;
  
  List<charts.Series<pRonda1, String>> _seriesData1 = [];
  List<charts.Series<pRonda2, String>> _seriesData2 = [];
  List<charts.Series<pRonda3, String>> _seriesData3 = [];
  List<PuntuacionJugador> puntuacionesrondaDesenlace = [];
  List<PuntuacionJugador> puntuacionesronda1 = [];
  List<PuntuacionJugador> puntuacionesronda2 = [];
  List<PuntuacionJugador> puntuacionesronda3 = [];
  List<pRonda1> cartasRonda1 = [];
  List<pRonda2> cartasRonda2 = [];
  List<pRonda3> cartasRonda3 = [];
  //List<charts.Series<Partida2, String>> _informacionPartida = [];
  List<int> puntos = [];
  List<String> nombres = [];
 _VistaDetallePartidaState(this.partida);

  @override
  void initState(){
    super.initState();
    calcularPuntuaciones();
    _ordenarPosiciones();
    _datosRonda1();
    _datosRonda2();
    _datosRonda3();
    
  }
  void calcularPuntuaciones(){
    puntuacionesronda1 = partida.puntuaciones(rondas: FasePuntuacion.ronda1);
    puntuacionesronda2 = partida.puntuaciones(rondas: FasePuntuacion.ronda2);
    puntuacionesronda3 = partida.puntuaciones(rondas: FasePuntuacion.ronda3);
    puntuacionesrondaDesenlace = partida.puntuaciones(rondas: FasePuntuacion.desenlace);
  }

_datosRonda1() {
    _seriesData1.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules ,
        id: '1',
        data: partida.puntuacionesRonda1,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda1 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorAzulR2)),
      ), 
    );
  }
  _datosRonda2() {
    _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules ,
        id: '2',
        data: partida.puntuacionesRonda2,
        seriesCategory: '21',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda2 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorAzulR2)),
      ), 
    );
    _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasVerdes ,
        id: '2',
        data: partida.puntuacionesRonda2,
        seriesCategory: '22',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda2 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorVerdeR3)),
      ), 
    );
  }
  _datosRonda3() {
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '31',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorAzulR2)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasVerdes,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '32',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorVerdeR3)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasRosas,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '33',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorRosaR3)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasNegras,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '34',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (pRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(colorNegroR3)),
      ), 
    );
  }

  _ordenarPosiciones(){
    puntuacionesrondaDesenlace.sort((a, b) => a.total.compareTo(b.total));
    var listaInvertida = puntuacionesrondaDesenlace.reversed;
    for(var i = 0; i < listaInvertida.length; i++){
        puntos.insert(i, listaInvertida.elementAt(i).total);
        nombres.insert(i, listaInvertida.elementAt(i).jugador.nombre);
      }
}

 _ganador(){
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Column(
        children: [ 
          Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 3.0),
            child: Container(
              //height: 30.0,
              decoration: BoxDecoration(
                color: Color(colorGanador),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Column(
                  children: [
                    FaIcon(
                      FontAwesomeIcons.crown, color: Color(colorPrincipal),
                    ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(nombres[0], 
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w900),
                          ),
                      ),
                    Text('Puntuacion: ' + puntos[0].toString(), 
                    style: TextStyle(
                      fontWeight: FontWeight.bold, 
                      color: Colors.black,
                      fontSize: 17.0
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ]
      ),
    );
  }

_perdedores(i){
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 3.0),
    child: Center(
      child: Container(
        //height: 30.0,
        decoration: BoxDecoration(
          color: Color(colorPerdedores),
          borderRadius: BorderRadius.circular(5),
    ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 3.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(nombres[i], 
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                          ),
                        ),
                ),
                Text('Puntuacion: '+ puntos[i].toString(),style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 4,
      child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(colorPrincipal),
        leading: IconButton(onPressed: (){
          Navigator.push( context, MaterialPageRoute(builder: (context) => VistaPartidas()));
        }, 
        icon: Icon(Icons.arrow_back)),
          title: const Text('Detalle de la partida'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(icon: FaIcon(
                      FontAwesomeIcons.crown, color: Colors.white,
                    )),
              Tab(
                icon: Icon(Icons.looks_one),
              ),
              Tab(
                icon: Icon(Icons.looks_two),
              ),
              Tab(
                icon: Icon(Icons.looks_3),
              ),
            ],
          ),
      ),
      body:TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: [
                    Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Lugares', 
                          style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.w800,
                          ),),
                        ),
                      ),
                    _ganador(),
                   for(int i = 1; i < nombres.length; i++) _perdedores(i)
                  ]
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Cartas jugadas',
                      style: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.w800,
                          color: Color(colorInputs) 
                        ),
                      ),
                    ),
                    Center(
                      child: _graficaRonda1(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text('Puntuaciones de ronda 1',
                      style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w800,
                        color: Color(colorInputs) 
                      ),
                      ),
                    ),
                    for(int i = 0; i < partida.jugadores.length; i++) _puntuacionRonda(i),
                    
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Cartas jugadas',
                      style: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.w800,
                          color: Color(colorInputs) 
                        ),
                      ),
                    ),
                    Center(
                      child: _graficaRonda2(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text('Puntuaciones de ronda 2',
                      style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w800,
                        color: Color(colorInputs) 
                      ),
                      ),
                    ),
                    for(int i = 0; i < partida.jugadores.length; i++) _puntuacionRonda2(i),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Cartas jugadas',
                      style: TextStyle(
                          fontSize: 25.0,
                          fontWeight: FontWeight.w800,
                          color: Color(colorInputs) 
                        ),
                      ),
                    ),
                    Center(
                      child: _graficaRonda3(),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text('Puntuaciones de ronda 3',
                      style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w800,
                        color: Color(colorInputs) 
                      ),
                      ),
                    ),
                    for(int i = 0; i < partida.jugadores.length; i++) _puntuacionRonda3(i),                 
                  ],
                ),
              ),
            ],
          ),
      ),
    );
  }

  _puntuacionFinal(i){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /*Container(
          child:
          Text(partida.puntuacionesRonda1.elementAt(i).cuantasAzules.toString()),
        ),*/
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Text(puntuacionesrondaDesenlace[i].jugador.nombre, style: estiloNombresDetalle,),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Posiciones'),
                Text(puntuacionesrondaDesenlace[i].total.toString()),
              ],
            ),
          )
      ],
    );
  }

  _puntuacionRonda(i){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /*Container(
          child:
          Text(partida.puntuacionesRonda1.elementAt(i).cuantasAzules.toString()),
        ),*/
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Text(puntuacionesrondaDesenlace[i].jugador.nombre, style: estiloNombresDetalle,),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Azules', style: estiloPuntuacionesAzules,),
                Text(puntuacionesronda1[i].porAzules.toString(), style: estiloPuntuacionesAzules),
              ],
            ),
          )
      ],
    );
  }

  _puntuacionRonda2(i){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /*Container(
          child:
          Text(partida.puntuacionesRonda1.elementAt(i).cuantasAzules.toString()),
        ),*/
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Text(puntuacionesrondaDesenlace[i].jugador.nombre, style: estiloNombresDetalle,),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Azules', style: estiloPuntuacionesAzules),
                Text(puntuacionesronda2[i].porAzules.toString(), style: estiloPuntuacionesAzules),
              ],
            ),
            
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Verdes', style: estiloPuntuacionesVerdes),
                Text(puntuacionesronda2[i].porVerdes.toString(), style: estiloPuntuacionesVerdes),
              ],
            ), 
          )
      ],
    );
  }


  _puntuacionRonda3(i){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /*Container(
          child:
          Text(partida.puntuacionesRonda1.elementAt(i).cuantasAzules.toString()),
        ),*/
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Text(puntuacionesrondaDesenlace[i].jugador.nombre, style: estiloNombresDetalle,),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Azules', style: estiloPuntuacionesAzules),
                Text(puntuacionesronda3[i].porAzules.toString(), style: estiloPuntuacionesAzules),
              ],
            ),
            
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Verdes', style: estiloPuntuacionesVerdes),
                Text(puntuacionesronda3[i].porVerdes.toString(), style: estiloPuntuacionesVerdes),
              ],
            ), 
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Rosas', style: estiloPuntuacionesRosas),
                Text(puntuacionesronda3[i].porRosas.toString(), style: estiloPuntuacionesRosas),
              ],
            ), 
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Puntaje Negras', style: estiloPuntuacionesNegras),
                Text(puntuacionesronda3[i].porNegras.toString(), style: estiloPuntuacionesNegras),
              ],
            ), 
          )
      ],
    );
  }

 _graficaRonda3(){
  return Container(
    height: 300.0,
    child: Column(
      children: [
        Expanded(
          child: charts.BarChart(
            _seriesData3,
            animate: true,
            barGroupingType: charts.BarGroupingType.groupedStacked,
            animationDuration: Duration(seconds: 1),
          ),
        ),
      ],
    ),
  );
}
 _graficaRonda2(){
  return Container(
    height: 300.0,
    child: Column(
      children: [
        Expanded(
          child: charts.BarChart(
            _seriesData2,
            animate: true,
            barGroupingType: charts.BarGroupingType.groupedStacked,
            animationDuration: Duration(seconds: 1),
          ),
        ),
      ],
    ),
  );
}
 _graficaRonda1(){
  return Container(
    height: 300.0,
    child: Column(
      children: [
        Expanded(
          child: charts.BarChart(
            _seriesData1,
            animate: true,
            barGroupingType: charts.BarGroupingType.groupedStacked,
            animationDuration: Duration(seconds: 1),
          ),
        ),
      ],
    ),
  );
}
}


