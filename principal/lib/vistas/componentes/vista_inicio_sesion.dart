import 'dart:async';

import 'package:db_paquete/db_paquete.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/local/local.dart';
import 'package:principal/vistas/componentes/componentes.dart';
import 'package:principal/vistas/componentes/estilos.dart';
import 'package:principal/vistas/componentes/vista_partidas.dart';

class VistaInicioSesion extends StatefulWidget {
  const VistaInicioSesion({Key? key}) : super(key: key);

  @override
  State<VistaInicioSesion> createState() => _VistaInicioSesionState();
}

class _VistaInicioSesionState extends State<VistaInicioSesion> {
  bool _isObscure = false;
  final tfCorreo = TextEditingController();
  final tfContrasena = TextEditingController();
  bool val = false;
  Local local = Local();
  Mongo mongo = Mongo();
  final _formKey = GlobalKey<FormState>();
  bool jugadorRegistrado = false;

  Future<bool> iniciarSesion() async {
    bool bandera = await mongo.iniciarMongo();
    if (bandera == true) {
      Jugadores jugador = Jugadores(
          nombre: '',
          correo: tfCorreo.text.toString(),
          contrasena: tfContrasena.text.toString(),
          partida: []);
      var valContra = await mongo.recuperarJugador(j: jugador);
      val = await mongo.inicioSesion(j: jugador);
      if (val == true) {
        // local.registrarJugadores(j: j);
        Jugadores j = await mongo.recuperarJugador(j: jugador);
        local.registrarJugadores(j: j);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => VistaPartidas()));
      } else {
        if (valContra.contrasena.toString() != tfContrasena) {
          print('La contrasena es incorrecta');
          _mensae('La contrasena es incorrecta');
        } 
        if(valContra.correo.toString() !=tfCorreo) {
          print('No existe');
          _mensae('El usuario no existe');
        }
      }
    } else {
      print('No hay conexion');
      _mensae('No hay conexion');
    }
    print(val);
    return val;
  }

  void entrarSinConexion() async {
    Jugadores jugador = Jugadores(
        nombre: '',
        correo: '',
        contrasena: '',
        partida: []);
    //local.registrarJugadores(j: jugador);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => VistaPartidas()));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: estiloThemeData,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => VistaIniciandome()));
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text("Iniciar Sesi\u00F3n"),
        backgroundColor: Color(colorPrincipal),
        ),
        body: Center(
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  imagenLogo,
                  width: 150,
                  height: 100,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    'Inicia Sesi\u00F3n',
                    style: estiloTitulos,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: StreamBuilder(builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          return Container(
                            child: TextFormField(
                              controller: tfCorreo,
                              validator: (value) => value == null ||
                                      value.isEmpty ||
                                      !value.contains("@")
                                  ? 'Correo Invalido'
                                  : null,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                hintText: 'maria@gmail.com',
                                labelText: 'Correo',
                                border: estiloOutlineBorder,
                                focusColor: Color(colorPrincipal),
                                //enabledBorder: estiloEnableBorder,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(colorPrincipal))),
                                hintStyle:
                                    TextStyle(color: Color(colorPrincipal)),
                              ),
                              onChanged: (value) {},
                            ),
                          );
                        }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: StreamBuilder(builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          return Container(
                              child: TextFormField(
                                  controller: tfContrasena,
                                  onChanged: (text){
                                  },
                                  validator: (value) =>
                                      value == null || value.isEmpty
                                          ? 'Ingrese su contrase\u00F1a'
                                          : null,
                                  obscureText: _isObscure ? false : true,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.deny(
                                        RegExp(r"\s\b|\b\s"))
                                  ],
                                  decoration: InputDecoration(
                                    hintText: 'Contrase\u00F1a',
                                    labelText: 'Contrase\u00F1a',
                                    focusColor: Color(colorPrincipal),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(colorInputs))),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(colorPrincipal))),
                                    hintStyle:
                                        TextStyle(color: Color(colorPrincipal)),
                                    //enabledBorder: estiloEnableBorder,
                                    suffixIcon: IconButton(
                                      onPressed: () => _togglePasswordView(),
                                      icon: Icon(_isObscure
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                    ),
                                  )));
                        }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          width: double.infinity,
                          height: 40,
                          child: StreamBuilder(builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            return ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color(colorBotones),
                                onPrimary: Colors.white,
                                //fixedSize: Size(, 40),
                                textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  iniciarSesion();
                                }
                              },
                              child: Text('Ingresar'),
                            );
                          }),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          width: double.infinity,
                          height: 40,
                          child: StreamBuilder(builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            return ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Color(colorBotones),
                                onPrimary: Colors.white,
                                //fixedSize: Size(, 40),
                                textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                ),
                              ),
                              onPressed: () async {
                                  entrarSinConexion();
                              },
                              child: Text('Ingresar sin internet'),
                            );
                          }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isObscure = !_isObscure;
    });
  }

  void _mensae(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(mensaje)));
  }
}
