import 'package:flutter/material.dart';
import 'package:principal/constantes/colores.dart';
import 'package:principal/vistas/componentes/estilos.dart';

class VistaAnadirPartidas extends StatefulWidget {
  const VistaAnadirPartidas({ Key? key }) : super(key: key);

  @override
  State<VistaAnadirPartidas> createState() => _VistaAnadirPartidasState();
}

class _VistaAnadirPartidasState extends State<VistaAnadirPartidas> {
  @override
  Widget build(BuildContext context) {
    List<int> jugadores = [1, 2, 3, 4];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(colorPrincipal),
        title: Text('A\u00F1adir Partidas'),
        ),
        body:  RawScrollbar(
          isAlwaysShown: true,
              thickness: 8,
              thumbColor: Color(colorPrincipal),
              radius: const Radius.circular(5),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('Ronda 1',
                    style: estiloTituloRondas,
                    ),
                  ),
                  for(var i in jugadores)_ronda1(jugadores[i-1]),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('Ronda 2',
                    style: estiloTituloRondas,
                    ),
                  ),
                  for(var i in jugadores)_ronda2(jugadores[i-1]),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('Ronda 3',
                    style: estiloTituloRondas,
                    ),
                  ),
                  for(var i in jugadores)_ronda3(jugadores[i-1]),
              ],
            ),
          ),
        ),
    );
  }
}

_ronda1(int numJugador){
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0 , bottom: 3.0),
    child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 5.0),
          child: Text('Jugador '+ numJugador.toString(),
          style: estiloTexto,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                focusColor: Color(colorPrincipal),
                labelText: 'Azules',
                hintText: "",
                border: borderInputsAnadir,
              ),
            ),
          ),
        ),  
      ],
    ),
  );
}

_ronda2(int numJugador){
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0 , bottom: 3.0),
    child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 5.0),
          child: Text('Jugador '+ numJugador.toString()),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
               maxLength: 2,
              decoration: InputDecoration(
                focusColor: Color(colorPrincipal),
                labelText: 'Azules',
                hintText: "",
                border: borderInputsAnadir,
              ),
            ),
          ),
        ), 
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                hintText: "",
                focusColor: Color(colorPrincipal),
                labelText: 'Verdes',
                border: borderInputsAnadir,
              ),
            ),
          ),
        ) 
      ],
    ),
  );
}

_ronda3(int numJugador){
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0 , bottom: 3.0),
    child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 5.0),
          child: Text('Jugador '+ numJugador.toString()),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                focusColor: Color(colorPrincipal),
                hintText: "",
                labelText: 'Azules',
                border: borderInputsAnadir,
              ),
            ),
          ),
        ), 
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                 hintText: "",
                focusColor: Color(colorPrincipal),
                labelText: 'Verdes',
                border: borderInputsAnadir,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 90.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                focusColor: Color(colorPrincipal),
                labelText: 'Rosas',
                border: borderInputsAnadir,
                hintText: "",
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3.0),
          child: SizedBox(
            width: 80.0,
            child: TextFormField(
              maxLength: 2,
              decoration: InputDecoration(
                labelText: 'Negras',
                border: borderInputsAnadir,
                counterText: "",
              ),
            ),
          ),
        ),
      ],
    ),
  );
}