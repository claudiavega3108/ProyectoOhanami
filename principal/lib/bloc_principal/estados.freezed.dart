// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'estados.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$Estado2TearOff {
  const _$Estado2TearOff();

  ronda1 Ronda1(Partida partida) {
    return ronda1(
      partida,
    );
  }

  ronda2 Ronda2(Partida partida) {
    return ronda2(
      partida,
    );
  }

  ronda3 Ronda3(Partida partida) {
    return ronda3(
      partida,
    );
  }
}

/// @nodoc
const $Estado2 = _$Estado2TearOff();

/// @nodoc
mixin _$Estado2 {
  Partida get partida => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Partida partida) Ronda1,
    required TResult Function(Partida partida) Ronda2,
    required TResult Function(Partida partida) Ronda3,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ronda1 value) Ronda1,
    required TResult Function(ronda2 value) Ronda2,
    required TResult Function(ronda3 value) Ronda3,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $Estado2CopyWith<Estado2> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Estado2CopyWith<$Res> {
  factory $Estado2CopyWith(Estado2 value, $Res Function(Estado2) then) =
      _$Estado2CopyWithImpl<$Res>;
  $Res call({Partida partida});
}

/// @nodoc
class _$Estado2CopyWithImpl<$Res> implements $Estado2CopyWith<$Res> {
  _$Estado2CopyWithImpl(this._value, this._then);

  final Estado2 _value;
  // ignore: unused_field
  final $Res Function(Estado2) _then;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(_value.copyWith(
      partida: partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc
abstract class $ronda1CopyWith<$Res> implements $Estado2CopyWith<$Res> {
  factory $ronda1CopyWith(ronda1 value, $Res Function(ronda1) then) =
      _$ronda1CopyWithImpl<$Res>;
  @override
  $Res call({Partida partida});
}

/// @nodoc
class _$ronda1CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ronda1CopyWith<$Res> {
  _$ronda1CopyWithImpl(ronda1 _value, $Res Function(ronda1) _then)
      : super(_value, (v) => _then(v as ronda1));

  @override
  ronda1 get _value => super._value as ronda1;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(ronda1(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$ronda1 with DiagnosticableTreeMixin implements ronda1 {
  const _$ronda1(this.partida);

  @override
  final Partida partida;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado2.Ronda1(partida: $partida)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado2.Ronda1'))
      ..add(DiagnosticsProperty('partida', partida));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ronda1 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $ronda1CopyWith<ronda1> get copyWith =>
      _$ronda1CopyWithImpl<ronda1>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Partida partida) Ronda1,
    required TResult Function(Partida partida) Ronda2,
    required TResult Function(Partida partida) Ronda3,
  }) {
    return Ronda1(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
  }) {
    return Ronda1?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda1 != null) {
      return Ronda1(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ronda1 value) Ronda1,
    required TResult Function(ronda2 value) Ronda2,
    required TResult Function(ronda3 value) Ronda3,
  }) {
    return Ronda1(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
  }) {
    return Ronda1?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda1 != null) {
      return Ronda1(this);
    }
    return orElse();
  }
}

abstract class ronda1 implements Estado2 {
  const factory ronda1(Partida partida) = _$ronda1;

  @override
  Partida get partida;
  @override
  @JsonKey(ignore: true)
  $ronda1CopyWith<ronda1> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ronda2CopyWith<$Res> implements $Estado2CopyWith<$Res> {
  factory $ronda2CopyWith(ronda2 value, $Res Function(ronda2) then) =
      _$ronda2CopyWithImpl<$Res>;
  @override
  $Res call({Partida partida});
}

/// @nodoc
class _$ronda2CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ronda2CopyWith<$Res> {
  _$ronda2CopyWithImpl(ronda2 _value, $Res Function(ronda2) _then)
      : super(_value, (v) => _then(v as ronda2));

  @override
  ronda2 get _value => super._value as ronda2;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(ronda2(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$ronda2 with DiagnosticableTreeMixin implements ronda2 {
  const _$ronda2(this.partida);

  @override
  final Partida partida;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado2.Ronda2(partida: $partida)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado2.Ronda2'))
      ..add(DiagnosticsProperty('partida', partida));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ronda2 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $ronda2CopyWith<ronda2> get copyWith =>
      _$ronda2CopyWithImpl<ronda2>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Partida partida) Ronda1,
    required TResult Function(Partida partida) Ronda2,
    required TResult Function(Partida partida) Ronda3,
  }) {
    return Ronda2(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
  }) {
    return Ronda2?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda2 != null) {
      return Ronda2(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ronda1 value) Ronda1,
    required TResult Function(ronda2 value) Ronda2,
    required TResult Function(ronda3 value) Ronda3,
  }) {
    return Ronda2(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
  }) {
    return Ronda2?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda2 != null) {
      return Ronda2(this);
    }
    return orElse();
  }
}

abstract class ronda2 implements Estado2 {
  const factory ronda2(Partida partida) = _$ronda2;

  @override
  Partida get partida;
  @override
  @JsonKey(ignore: true)
  $ronda2CopyWith<ronda2> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ronda3CopyWith<$Res> implements $Estado2CopyWith<$Res> {
  factory $ronda3CopyWith(ronda3 value, $Res Function(ronda3) then) =
      _$ronda3CopyWithImpl<$Res>;
  @override
  $Res call({Partida partida});
}

/// @nodoc
class _$ronda3CopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ronda3CopyWith<$Res> {
  _$ronda3CopyWithImpl(ronda3 _value, $Res Function(ronda3) _then)
      : super(_value, (v) => _then(v as ronda3));

  @override
  ronda3 get _value => super._value as ronda3;

  @override
  $Res call({
    Object? partida = freezed,
  }) {
    return _then(ronda3(
      partida == freezed
          ? _value.partida
          : partida // ignore: cast_nullable_to_non_nullable
              as Partida,
    ));
  }
}

/// @nodoc

class _$ronda3 with DiagnosticableTreeMixin implements ronda3 {
  const _$ronda3(this.partida);

  @override
  final Partida partida;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Estado2.Ronda3(partida: $partida)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Estado2.Ronda3'))
      ..add(DiagnosticsProperty('partida', partida));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ronda3 &&
            const DeepCollectionEquality().equals(other.partida, partida));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(partida));

  @JsonKey(ignore: true)
  @override
  $ronda3CopyWith<ronda3> get copyWith =>
      _$ronda3CopyWithImpl<ronda3>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Partida partida) Ronda1,
    required TResult Function(Partida partida) Ronda2,
    required TResult Function(Partida partida) Ronda3,
  }) {
    return Ronda3(partida);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
  }) {
    return Ronda3?.call(partida);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Partida partida)? Ronda1,
    TResult Function(Partida partida)? Ronda2,
    TResult Function(Partida partida)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda3 != null) {
      return Ronda3(partida);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ronda1 value) Ronda1,
    required TResult Function(ronda2 value) Ronda2,
    required TResult Function(ronda3 value) Ronda3,
  }) {
    return Ronda3(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
  }) {
    return Ronda3?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ronda1 value)? Ronda1,
    TResult Function(ronda2 value)? Ronda2,
    TResult Function(ronda3 value)? Ronda3,
    required TResult orElse(),
  }) {
    if (Ronda3 != null) {
      return Ronda3(this);
    }
    return orElse();
  }
}

abstract class ronda3 implements Estado2 {
  const factory ronda3(Partida partida) = _$ronda3;

  @override
  Partida get partida;
  @override
  @JsonKey(ignore: true)
  $ronda3CopyWith<ronda3> get copyWith => throw _privateConstructorUsedError;
}
