import 'package:paquete/paquete.dart';

class Evento{}

class DespuesRonda1 extends Evento{
  final Partida partida;

  DespuesRonda1({required this.partida});
}

class DespuesRonda2 extends Evento{
  final Partida partida;

  DespuesRonda2({required this.partida});
}

class DespuesRonda3 extends Evento{
  final Partida partida;

  DespuesRonda3({required this.partida});
}