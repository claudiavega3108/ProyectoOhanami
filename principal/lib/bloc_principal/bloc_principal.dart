import 'package:bloc/bloc.dart';
import 'package:paquete/paquete.dart';
import 'package:principal/bloc_principal/estados.dart';
import 'package:principal/bloc_principal/eventos.dart';

class BlocPrincipal extends Bloc<Evento, Estado2>{
  BlocPrincipal(this.partida) : super(ronda1(partida)){
    on<DespuesRonda1>(_onRonda1);
    on<DespuesRonda2>(_onRonda2);
    on<DespuesRonda3>(_onRonda3);
  }
  Partida partida;

  void _onRonda1(DespuesRonda1 evento, Emitter<Estado2> emit){
    partida = evento.partida;
    emit(ronda1(partida));
  }

  void _onRonda2(DespuesRonda2 evento, Emitter<Estado2> emit){
    partida = evento.partida;
    emit(ronda2(partida));
  }

  void _onRonda3(DespuesRonda3 evento, Emitter<Estado2> emit){
    partida = evento.partida;
    emit(ronda3(partida));
  }
}

