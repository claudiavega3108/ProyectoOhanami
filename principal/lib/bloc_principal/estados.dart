import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:paquete/paquete.dart';

part 'estados.freezed.dart';

/*class Estado{}

class Ronda1 extends Estado{}
class Ronda2 extends Estado{}
class Ronda3 extends Estado{}
*/
@freezed
class Estado2 with _$Estado2{
  const factory Estado2.Ronda1(Partida partida) = ronda1;
  const factory Estado2.Ronda2(Partida partida) = ronda2;
  const factory Estado2.Ronda3(Partida partida) = ronda3;
}
