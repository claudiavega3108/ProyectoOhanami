import 'dart:async';

import 'package:principal/local/local.dart';
import 'package:flutter/material.dart';
import 'package:principal/vistas/componentes/componentes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:db_paquete/db_paquete.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Ohanami',
      debugShowCheckedModeBanner: false,
      home: VistaIniciandome(),
    );
  }
}