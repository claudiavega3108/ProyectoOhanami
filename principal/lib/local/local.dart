import 'dart:convert';
import 'package:db_paquete/db_paquete.dart';
import 'package:paquete/paquete.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Local{

  Future<bool> valExisteJugador() async {
    late bool bandera;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var res =  prefs.getString('usuarios');
    if (res == null){
     return bandera = false;
    }
    return bandera = true;
  }

  Future<bool> registrarPartida({required Partida partida}) async {
    bool bandera;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var res =  prefs.getString('usuarios');
    var jugadoresJson = jsonDecode(res!);
    Jugadores jugadores = Jugadores.fromJson(jugadoresJson);
    jugadores.partida.add(partida);
    bandera = await registrarJugadores(j: jugadores);
    return bandera;
  }

  Future<List<Partida>> regresarPartidas() async {
    List<Partida> partidas;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta =  prefs.getString('usuarios');
    var jugadoresJson = jsonDecode(respuesta!);
    Jugadores jugadores = Jugadores.fromJson(jugadoresJson);
    return partidas = jugadores.partida;
  }

    Future<bool> cambiarJugador({required Jugadores usuarioNuevo}) async{
    bool bandera = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var res =  prefs.getString('usuarios');
    var jugadorJson = jsonDecode(res!);
    Jugadores jugadorAnterior = Jugadores.fromJson(jugadorJson);
    jugadorAnterior.nombre = usuarioNuevo.nombre;
    jugadorAnterior.contrasena = usuarioNuevo.contrasena;
    jugadorAnterior.correo = usuarioNuevo.correo;
    bandera = await  reescribirJugadores(j: jugadorAnterior);
    return bandera;
  }

  Future<bool> registrarJugadores({required Jugadores j}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool bandera;
    bandera = await prefs.setString('usuarios', jsonEncode(j.toJson())).then((bool success) {
        return success;
      });
    return bandera;
  }
  
  Future<Jugadores> recuperarJugadores() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var res =  prefs.getString('usuarios');
    var jugadoresJson = jsonDecode(res!);
    Jugadores jugadores = Jugadores.fromJson(jugadoresJson);
    return jugadores;
  }
    
  Future<bool> reescribirJugadores({required Jugadores j})async{
    bool bandera = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bandera = await prefs.clear().then((bool success) {
        return success;
      });
    if(bandera == true){
      bandera = await prefs.setString('usuarios', jsonEncode(j.toJson())).then((bool success){
        return success;
      });
    }
    return bandera;
  } 

  Future<bool> eliminarJugadores() async {
    bool bandera = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bandera = await prefs.clear().then((bool success) {
        return success;
      });
    return bandera;
  }


  Future<bool> eliminarPartida({required int index}) async{
    bool bandera = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta =  prefs.getString('usuarios');
    var usuarioJson = jsonDecode(respuesta!);
    Jugadores jugadorAnterior = Jugadores.fromJson(usuarioJson);
    jugadorAnterior.partida.removeAt(index);
    bandera = await  reescribirJugadores(j: jugadorAnterior);
    return bandera;
  } 

}

