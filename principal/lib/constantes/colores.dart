const colorPrincipal = 0xFFB5179E;
const colorInputs = 0xFF4E3E3B;
const colorInputsRI = 0xFF4895EF;
const colorBotones = 0xFF560BAD;
const bienvenidaMensaje = 0xFF3A0CA3;
const colorAzulR1 = 0xFF337CFF;
const colorAzulR2 = 0xFF1736C7;
const colorAzulR3 = 0xFF102587;
const colorVerdeR2 = 0xFF14D20A;
const colorVerdeR3 = 0xFF1A7431;
const colorNegroR3 = 0xFF111510;
const colorRosaR3 = 0xFFF92A82;
const colorGanador = 0xFFFCEC1E;
const colorPerdedores = 0xFF919095;
const String imagenLogo = 'assets/images/ohanami.png';

//Morado 0xFF7209B7
//Azul cielo 0xFF4CC9F0
//Azul rey 0xFF4361EE
// Lila grisaseo 0xFF8388AE