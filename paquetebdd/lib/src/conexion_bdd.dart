import 'dart:io';

import 'package:db_paquete/src/constantes.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:paquete/paquete.dart';
import 'dart:convert';

import 'repo_datos.dart';
import 'jugador.dart';

class Mongo extends RepoDatos{
  @override

  Future<bool> iniciarMongo() async{
  bool check = false;
    try {
      await Db.create(conexion).then((value) => check = true);
    } on SocketException catch (_) {
      check = false;
    }
      return check;
    }

  Future<bool> registrarJugadores({required Jugadores jugador}) async {
    var bandera = false;
    var db = await Db.create(conexion);
    await db.open();
    var col = db.collection('usuarios');
    await col.insert(jsonDecode(jugador.toJson())).then((value) => bandera =  true);
    db.close();
    return bandera;
  }

  Future <bool> valExisteJugador({required Jugadores jugador}) async{
    var db = await Db.create(conexion);
    await db.open();
    var col = db.collection('usuarios');
    var usuarioExiste = await col.find(where.eq('nombre', jugador.nombre.toString())).toList();
    var val;
    db.close();
    if(usuarioExiste.isEmpty){
      val =  false;
    }
    else{
      val =  true;
    }
    return val;
  }

  Future<bool> ingresarPartida({required Jugadores j, required Partida p}) async{
    var bandera = false;
    var db = await Db.create(conexion);
    await db.open();
    var col = db.collection('usuarios');
    var partidas = jsonDecode(p.toJson());
    List<Partida> lista = await recuperarPartidas(j: j);
    if(lista.isEmpty){
      await col.update(await col.findOne(where.eq('nombre', j.nombre.toString())), 
      SetStage({
        'partida': [partidas],
      }).build()
      ).then((value) => bandera = true);
    }
    await col.update(await col.findOne(where.eq('nombre', j.nombre.toString())), 
    Push({
      'partida': [partidas],
    }).build()
    ).then((value) => bandera = true);
    await db.close();
    return bandera;
  }

  Future<List<Partida>> recuperarPartidas({required Jugadores j}) async {
    List<Partida> partidas;
    var db = await Db.create(conexion);
    await db.open();
    var colexion = db.collection('usuarios');
    var val = await colexion.findOne(where.eq('nombre', j.nombre.toString()));
    await db.close();
    var res = val!.remove('_id');
    Jugadores x = Jugadores.fromMap(val);
    partidas = x.partida;
    return partidas;
  }

  Future<bool> eliminarJugador({required Jugadores j}) async {
    var bandera = false;
    var db = await Db.create(conexion);
    await db.open();
    var col = db.collection('usuarios'); 
    var elim = await col.deleteOne(where.eq('nombre', j.nombre.toString())).then((value) => bandera = true);
    db.close();
    return bandera;
  }

  @override
  Future<bool> inicioSesion({required Jugadores j}) async{
    var db = await Db.create(conexion);
    await db.open();
    var col = db.collection('usuarios');
    var val = await col.findOne(where.eq('correo', j.correo.toString()).and(where.eq('contrasena', j.contrasena.toString())));
    await db.close();
    if(val == null){
      return false;
    }
    else{
      return true;
    }
  }

  Future<bool> actualizarPartida({required Jugadores j}) async{
    bool bandera = false;
    List<Partida> partidas = j.partida;
    var partidas2 = jsonDecode(json.encode(partidas.map((x) => x.toMap()).toList()));
    var db = await Db.create(conexion);
    await db.open();
    var colexion = db.collection('usuarios');
    await colexion.updateOne(where.eq('nombre', j.nombre.toString()), modify.set('partida', partidas2));
    db.close();
    return bandera;

  }

   Future<Jugadores> recuperarJugador({ required Jugadores j}) async {
    var db = await Db.create(conexion);
    await db.open();
    var colexion = db.collection('usuarios');
    var val = await colexion.findOne(where.eq('correo', j.correo.toString()));
    await db.close();
    val!.remove('_id');
    Jugadores x = Jugadores.fromMap(val);
    return x;
  }

  Future<bool> sincronizarUsuario({required Jugadores j}) async{
    bool badera = await eliminarJugador(j: j);
    if (badera == true) {
     bool vaal = await registrarJugadores(jugador: j);
     badera = vaal;
    }
    return badera;
  }


}