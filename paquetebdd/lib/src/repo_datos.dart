import 'package:paquete/paquete.dart';

import 'jugador.dart';
 
abstract class RepoDatos{
  Future<bool> registrarJugadores({required Jugadores jugador});
  Future <bool> valExisteJugador({required Jugadores jugador});
  Future<bool> ingresarPartida({required Jugadores j, required Partida p});
  Future<List<Partida>> recuperarPartidas({required Jugadores j});
  Future<bool> eliminarJugador({required Jugadores j});
  Future<bool> inicioSesion({required Jugadores j});
  Future<bool> actualizarPartida({required Jugadores j});
  Future<bool> iniciarMongo();

}