import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:paquete/paquete.dart';

class Jugadores {
  String nombre;
  String correo;
  String contrasena;
  List<Partida> partida;
  Jugadores({
    required this.nombre,
    required this.correo,
    required this.contrasena,
    required this.partida,
  });

  Jugadores copyWith({
    String? nombre,
    String? correo,
    String? contrasena,
    List<Partida>? partida,
  }) {
    return Jugadores(
      nombre: nombre ?? this.nombre,
      correo: correo ?? this.correo,
      contrasena: contrasena ?? this.contrasena,
      partida: partida ?? this.partida,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'correo': correo,
      'contrasena': contrasena,
      'partida': partida.map((x) => x.toMap()).toList(),
    };
  }

  factory Jugadores.fromMap(Map<String, dynamic> map) {
    return Jugadores(
      nombre: map['nombre'],
      correo: map['correo'],
      contrasena: map['contrasena'],
      partida: List<Partida>.from(map['partida']?.map((x) => Partida.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Jugadores.fromJson(String source) => Jugadores.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Jugadores(nombre: $nombre, correo: $correo, contrasena: $contrasena, partida: $partida)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;
  
    return other is Jugadores &&
      other.nombre == nombre &&
      other.correo == correo &&
      other.contrasena == contrasena &&
      listEquals(other.partida, partida);
  }

  @override
  int get hashCode {
    return nombre.hashCode ^
      correo.hashCode ^
      contrasena.hashCode ^
      partida.hashCode;
  }
}
