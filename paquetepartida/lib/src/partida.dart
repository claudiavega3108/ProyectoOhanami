import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:paquete/src/helpers.dart';
import 'package:paquete/src/problemas.dart';
import 'package:paquete/src/puntuacion_jugador.dart';
import 'package:paquete/src/puntuaciones.dart';

import '../paquete.dart';

const numeroMinimoJugadores = 2;
const numeroMaximoJugadores = 4;
const puntosPorAzul = 3;
const puntosPorVerde = 4;
const puntosPorNegra = 7;
var puntosPorRosa = [0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120].toList();

enum  FasePuntuacion {ronda1, ronda2, ronda3, desenlace}


class Partida {
    final Set<Jugador> jugadores;
    List<pRonda1> puntuacionesRonda1 = [];
    List<pRonda2> puntuacionesRonda2 = [];
    List<pRonda3> puntuacionesRonda3 = [];
  Partida({
    required this.jugadores,
  }){
    if(jugadores.length < numeroMinimoJugadores) throw ProblemaNumeroJugadoresMenorQueMinimo();
    if(jugadores.length > numeroMaximoJugadores) throw ProblemaJugadoresMayorQueMaximo();
  }

  Partida.constructor({
    required this.jugadores,
    required this.puntuacionesRonda1,
    required this.puntuacionesRonda2,
    required this.puntuacionesRonda3,
  });


///Guarda los datos de la primera ronda de puntuacion de Ohanami
///
///En caso que los jugadores de la puntuacion no coincidan con los 
///del juego tira [ProblemaJugadoresNoConcuerdan]
///
///No se puede ingresar mas de 10 cartas, de lo contrario arrojara [ProblemasDemasiadasAzules]
    List<PuntuacionJugador> puntuaciones( {required FasePuntuacion rondas}){
      List<PuntuacionJugador> _puntuacionJugador = [];

      switch (rondas) {
        case FasePuntuacion.ronda1:

        for (var pRonda1 in puntuacionesRonda1) {
          Jugador jugador = pRonda1.jugador;
          int azules = pRonda1.cuantasAzules;
          _puntuacionJugador.add(PuntuacionJugador(
            jugador: jugador, 
            porAzules: azules * puntosPorAzul, 
            porVerdes: 0, 
            porRosas: 0, 
            porNegras: 0));
        }
        return _puntuacionJugador;

        case FasePuntuacion.ronda2:

        for (var pRonda2 in puntuacionesRonda2) {
          Jugador jugador = pRonda2.jugador;
          int azules = pRonda2.cuantasAzules;
          int verdes = pRonda2.cuantasVerdes;
          _puntuacionJugador.add(PuntuacionJugador(
            jugador: jugador, 
            porAzules: azules * puntosPorAzul, 
            porVerdes: verdes * puntosPorVerde, 
            porRosas: 0, 
            porNegras: 0));
        }
        return _puntuacionJugador;

        case FasePuntuacion.ronda3:

        for (var pRonda3 in puntuacionesRonda3) {
          Jugador jugador = pRonda3.jugador;
          int azules = pRonda3.cuantasAzules;
          int verdes = pRonda3.cuantasVerdes;
          int negras = pRonda3.cuantasNegras;
          int cantidadRosas = pRonda3.cuantasRosas;
          int rosas = puntosPorRosa[cantidadRosas].toInt();
          
          _puntuacionJugador.add(PuntuacionJugador(
            jugador: jugador,
            porAzules: azules * puntosPorAzul, 
            porVerdes: verdes * puntosPorVerde, 
            porRosas:  rosas > 15 ? 120 : puntosPorRosa[rosas],
            porNegras: negras * puntosPorNegra));
        }
        return _puntuacionJugador;

        case FasePuntuacion.desenlace:

        for( Jugador j in jugadores){
          int azules = puntuaciones(rondas: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntuaciones(rondas: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntuaciones(rondas: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules;
          int verdes = puntuaciones(rondas: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntuaciones(rondas: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntuaciones(rondas: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes;
          int negras = puntuaciones(rondas: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntuaciones(rondas: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntuaciones(rondas: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras;
          int rosas = puntuaciones(rondas: FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntuaciones(rondas: FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntuaciones(rondas: FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas;
          _puntuacionJugador.add(PuntuacionJugador(jugador: j, porAzules: azules, porVerdes: verdes, porRosas: rosas, porNegras: negras));
        }
        return _puntuacionJugador;
      }
  }
///Guarda los datos de la primera ronda de puntuacion de Ohanami
///
///En caso que los jugadores de la puntuacion no coincidan con los 
///del juego tira [ProblemaJugadoresNoConcuerdan]
///
///No se puede ingresar mas de 10 cartas, de lo contrario arrojara [ProblemasDemasiadasAzules]
    void cartasRonda1(List <pRonda1> puntuaciones){
      Set<Jugador> jugadoresR1 = puntuaciones.map((e) => e.jugador).toSet();
      if(!setEquals(jugadoresR1, jugadores)) throw ProblemaJugadoresNoConcuerdan();

      puntuacionesRonda1 = puntuaciones;
    }

///Guarda los datos de la segunda ronda de puntuacion de Ohanami
///
///En caso que los jugadores de la puntuacion no coincidan con los 
///del juego tira [ProblemaJugadoresNoConcuerdan]
///
///[cartasRonda2] debe ser llamada despues de [cartasRonda1],
/// de lo contrario arrojara [ProblemaOrdenIncorrecto]
/// 
/// No se puede ingresar mas de 20 en total, de lo contrario arrojara [ProblemasDemasiadasCartas]
/// 
///No se puede ingresar un numero menor de cartas al de la ronda anterior, 
///puesto que lanzara [ProblemaDisminucionAzules]
    void cartasRonda2(List<pRonda2> puntuaciones){
      const numeroMaximoCartas = 20;

      if(puntuacionesRonda1.isEmpty) throw ProblemaOrdenIncorrecto();
      Set<Jugador> jugadoresR2 =  puntuaciones.map((e) => e.jugador).toSet();
      if(!setEquals(jugadores, jugadoresR2)) throw ProblemaJugadoresNoConcuerdan();
      
      for(pRonda2 segundaPuntuacion in puntuaciones){
        pRonda1 primeraPuntuacion = 
            puntuacionesRonda1.firstWhere((element) => element.jugador == segundaPuntuacion.jugador);
        if(primeraPuntuacion.cuantasAzules > segundaPuntuacion.cuantasAzules){
          throw ProblemaDisminucionAzules();
        }
      }

      for(pRonda2 p in puntuaciones){
        if(p.cuantasAzules > numeroMaximoCartas) throw ProblemasDemasiadasAzules();
        if(p.cuantasVerdes > numeroMaximoCartas) throw ProblemasDemasiadasVerdes();
        if((p.cuantasVerdes + p.cuantasAzules) > numeroMaximoCartas) throw ProblemasDemasiadasCartas();
      }

      puntuacionesRonda2 = puntuaciones;
    }

///Guarda los datos de la tercera ronda de puntuacion de Ohanami
///
///En caso que los jugadores de la puntuacion no coincidan con los 
///del juego tira [ProblemaJugadoresNoConcuerdan]
///[cartasRonda3] debe ser llamada despues de [cartasRonda2],
/// de lo contrario arrojara [ProblemaOrdenIncorrecto]
/// 
/// No se puede ingresar mas de 20 en total, de lo contrario arrojara [ProblemasDemasiadasCartas]
/// 
///No se puede ingresar un numero menor de cartas al de la ronda anterior, 
///puesto que lanzara [ProblemaDisminucionAzules] y/o [ProblemaDisminucionVerdes]
    void cartasRonda3(List<pRonda3> puntuaciones){
      const numeroMaximoCartas = 30;

      if(puntuacionesRonda2.isEmpty) throw ProblemaOrdenIncorrecto();
      Set<Jugador> jugadoresR3 = puntuaciones.map((e) => e.jugador).toSet();
      if(!setEquals(jugadores, jugadoresR3)) throw ProblemaJugadoresNoConcuerdan();

      for(pRonda3 terceraPuntuacion in puntuaciones){
        pRonda2 segundaPuntuacion = 
            puntuacionesRonda2.firstWhere((element) => element.jugador == terceraPuntuacion.jugador);
        if(segundaPuntuacion.cuantasAzules > terceraPuntuacion.cuantasAzules){
          throw ProblemaDisminucionAzules();
        }
        if(segundaPuntuacion.cuantasVerdes > terceraPuntuacion.cuantasVerdes){
          throw ProblemaDisminucionVerdes();
        }
      }

      for(pRonda3 p in puntuaciones){
        if(p.cuantasAzules > numeroMaximoCartas) throw ProblemasDemasiadasAzules();
        if(p.cuantasVerdes > numeroMaximoCartas) throw ProblemasDemasiadasVerdes();
        if(p.cuantasRosas > numeroMaximoCartas) throw ProblemaDemasiadasRosas();
        if(p.cuantasNegras > numeroMaximoCartas) throw ProblemaDemasiadasNegras();
        if((p.cuantasVerdes + p.cuantasAzules + p.cuantasRosas + p.cuantasNegras) > numeroMaximoCartas) throw ProblemasDemasiadasCartas();
      }

      puntuacionesRonda3 = puntuaciones;
    }

  Partida copyWith({
    Set<Jugador>? jugadores,
    List<pRonda1>? puntuacionesRonda1,
    List<pRonda2>? puntuacionesRonda2,
    List<pRonda3>? puntuacionesRonda3,
  }) {
    return Partida.constructor(
      jugadores: jugadores ?? this.jugadores,
      puntuacionesRonda1: puntuacionesRonda1 ?? this.puntuacionesRonda1,
      puntuacionesRonda2: puntuacionesRonda2 ?? this.puntuacionesRonda2,
      puntuacionesRonda3: puntuacionesRonda3 ?? this.puntuacionesRonda3,

      
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugadores': jugadores.map((x) => x.toMap()).toList(),
      'puntuacionesRonda1': puntuacionesRonda1.map((x) => x.toMap()).toList(),
      'puntuacionesRonda2': puntuacionesRonda2.map((x) => x.toMap()).toList(),
      'puntuacionesRonda3': puntuacionesRonda3.map((x) => x.toMap()).toList(),
    };
  }

  factory Partida.fromMap(Map<String, dynamic> map) {
    return Partida.constructor(
      jugadores: Set<Jugador>.from(map['jugadores']?.map((x) => Jugador.fromMap(x))),
      puntuacionesRonda1: List<pRonda1>.from(map['puntuacionesRonda1']?.map((x) => pRonda1.fromMap(x))),
      puntuacionesRonda2: List<pRonda2>.from(map['puntuacionesRonda2']?.map((x) => pRonda2.fromMap(x))),
      puntuacionesRonda3: List<pRonda3>.from(map['puntuacionesRonda3']?.map((x) => pRonda3.fromMap(x))),

    );
  }

  String toJson() => json.encode(toMap());

  factory Partida.fromJson(String source) => Partida.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Partida(jugadores: $jugadores, puntuacionesRonda1: $puntuacionesRonda1, puntuacionesRonda2: $puntuacionesRonda2, puntuacionesRonda3: $puntuacionesRonda3)';
  }


  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final collectionEquals = const DeepCollectionEquality().equals;
  
    return other is Partida &&
      collectionEquals(other.jugadores, jugadores) &&
      collectionEquals(other.puntuacionesRonda1, puntuacionesRonda1) &&
      collectionEquals(other.puntuacionesRonda2, puntuacionesRonda2) &&
      collectionEquals(other.puntuacionesRonda3, puntuacionesRonda3);

  }

  @override
  int get hashCode {
    return jugadores.hashCode ^
      puntuacionesRonda1.hashCode ^
      puntuacionesRonda2.hashCode ^
      puntuacionesRonda3.hashCode;

  }
}
