class ProblemaNombreJugadorVacio extends Error implements Exception{
  @override
  String toString() => 'Todos los jugadores deben tener un nombre';
}

class ProblemaNumeroJugadoresMenorQueMinimo extends Error implements Exception{
  @override
  String toString() => 'Se necesitan m\u00E1s jugadores para iniciar una partida';
}

class ProblemaJugadoresMayorQueMaximo extends Error implements Exception{
  @override
  String toString() => 'Hay demasiados jugadores en esta partida';
}

class ProblemaJugadoresRepetidos extends Error implements Exception{
  @override
  String toString() => 'Los jugadores no pueden tener el mismo nombre';
}

class ProblemasAzulesNegativas extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas azules no puede ser negativo';
}

class ProblemasDemasiadasCartas extends Error implements Exception{
  @override
  String toString() => 'La suma de cartas por jugador excede el limite para esta ronda';
}

class ProblemaJugadoresNoConcuerdan extends Error implements Exception{
  @override
  String toString() => 'Los jugadores a puntuar no concuerdan';
}

class ProblemaVerdesNegativas extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas verdes no puede ser negativo';
}

class ProblemaOrdenIncorrecto extends Error implements Exception{
  @override
  String toString() => 'Los jugadores a puntuar no concuerdan';
}

class ProblemaAzulesMenores extends Error implements Exception{
  @override
  String toString() => 'El numero ingresado para cartas azules no puede ser menor a la ronda anterior';
}

class ProblemasDemasiadasAzules extends Error implements Exception{
  @override
  String toString() => 'La suma de cartas por jugador excede el limite para esta ronda';
}

class ProblemaNegrasNegativas extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas negras no puede ser negativo';
}

class ProblemaRosasNegativas extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas rosas no puede ser negativo';
}

class ProblemasDemasiadasVerdes extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas verdes sobrepasa el limite permitido para esta ronda ';
}

class ProblemaDisminucionAzules extends Error implements Exception{
  @override
  String toString() => 'El numero que esta ingresando para cartas azules no puede ser menor a la ronda anterior';
}

class ProblemaDisminucionVerdes extends Error implements Exception{
  @override
  String toString() => 'El numero que esta ingresando para cartas verdes no puede ser menor a la ronda anterior';
}

class ProblemaDemasiadasRosas extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas rosas sobrepasa el limite permitido para esta ronda  ';
}

class ProblemaDemasiadasNegras extends Error implements Exception{
  @override
  String toString() => 'El numero de cartas negras sobrepasa el limite permitido para esta ronda ';
}